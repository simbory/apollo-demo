package com.example.apollodemo.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RefreshScope
public class HomeController {
    private final String content;

    public HomeController(@Value("${demo.content}") String content) {
        this.content = content;
    }

    @GetMapping("/")
    public String index() {
        return content;
    }
}
